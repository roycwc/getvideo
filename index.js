const glob = require('glob');
const fs = require('fs');
const rimraf = require('rimraf');

let inputPath = process.argv[2];
let outputPath = process.argv[3];

const files = glob.sync("**/*.{mp4,avi,wmv,mkv,MP$,AVI,WMV,MKV}", { cwd: inputPath })

files
  .map(it => `${inputPath}/${it}`)
  .filter(it => {
    const stat = fs.statSync(it);
    const size = stat.size / 1024 / 1024;
    return size > 300
  })
  .map(it => {
    const filename = it.split('/').pop();
    console.log('moving', it, 'to', `${outputPath}/${filename}`)
    fs.renameSync(it, `${outputPath}/${filename}`)
    return it
  })
  .map(it => {
    const dir = it.split('/').slice(0, -1).join('/')
    if (dir === inputPath) return;
    console.log('removing', dir)
    rimraf.sync(dir)
  })
console.log('done')